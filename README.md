
# Code source du site de Rémi Borfigat

Ce dépôt contient le code source du site. Cependant, les fichiers de contenu — comme les images — sont vides pour garder intact la structure.

## Comment l’utiliser pour votre site ?

1. Cloner le répertoire sur votre machine avec `git`.
2. Modifier la structure pour l’adapter à votre contenu.
3. Poussez l’ensemble de votre répertoire sur la machine de votre hébergeur.

## Problèmes

Si vous rencontrez un quelconque problème, n’hésitez pas à me le [signaler par courriel](https://www.borfigat.org/pages/contact.html).

## Licence

Consulter le fichier LICENCE.txt de ce répertoire.
